package com.depixed.fileupload;

import android.util.Log;
import android.os.Bundle;
import io.flutter.plugin.common.ActivityLifecycleListener;
import io.flutter.app.FlutterActivity;
import io.flutter.plugins.GeneratedPluginRegistrant;
import android.content.Intent;
import java.util.HashMap;
import java.util.Map;
import java.io.File;
import java.net.URI;
import android.net.Uri;
import java.nio.ByteBuffer;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugins.share.FlutterShareReceiverActivity;

public class MainActivity extends FlutterShareReceiverActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GeneratedPluginRegistrant.registerWith(this);
  }
}
