import 'package:fileupload/models/MessageMode.dart';
import 'package:fileupload/tab/MessageCell.dart';
import 'package:flutter/material.dart';
import 'package:fileupload/globals.dart';
import 'package:provider/provider.dart';

class MessageScreen extends StatefulWidget {
  // method() => createState().uploadFile();

  @override
  _MessageScreenState createState() => _MessageScreenState();
}

class _MessageScreenState extends State<MessageScreen> {
  @override
  void initState() {
    super.initState();
    //_products.add('Laptop');

    Future.delayed(Duration.zero, () {
      getMessage();
      Provider.of<LoginBlock>(context).getMessageCouner();
    });
  }
  void dispose() {
    
    print('dispose');
  }

  Future getMessage() async {
    var bloc = Provider.of<LoginBlock>(context);
    //bloc.setDocIsLoading(true);
    bloc.readAllMessage();
    await bloc.getMessage();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var bloc = Provider.of<LoginBlock>(context);
    List<MessageModel> msgList = bloc.msgList.reversed.toList();

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Preferences.appColor,
        title: Text('Message'),
      ),
      backgroundColor: Colors.blueGrey[100],
      body: bloc.docIsLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : MessageCell(msgList),
    );
    

  }
}
