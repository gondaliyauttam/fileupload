import 'package:fileupload/models/FileUplodDialog.dart';
import 'package:fileupload/models/document.model.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:documents_picker/documents_picker.dart';
import 'package:fileupload/globals.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';
import './../models/document.model.dart';
import 'package:flutter/services.dart';
import 'package:share/receive_share_state.dart';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:progress_hud/progress_hud.dart';

class FirstTab extends StatefulWidget {
  // method() => createState().uploadFile();

  @override
  _FirstTabState createState() => _FirstTabState();
}

class _FirstTabState extends ReceiveShareState<FirstTab> {

  ProgressHUD _progressHUD;

  String dropdownValue;
  String lastSelectedValue;
  File _image;
  List<String> docPaths;
  List<Map<String, dynamic>> docList = [];

  static const platform = const MethodChannel('app.channel.shared.data');
  Map<dynamic, dynamic> sharedData = Map();
  @override
  void initState() {
    super.initState();

    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black38,
      color: Colors.white,
      containerColor: Colors.blue,
      borderRadius: 5.0,
      text: 'Loading...',
    );



  }

  String _shared = '';
  @override
  Future<void> receiveShare(Share shared) async {
    // TODO: implement receiveShare
    
    debugPrint("Share received - $shared");
    
    //setState(() async {
      _shared = shared.path; //.toString();
      final filePath = await FlutterAbsolutePath.getAbsolutePath(_shared);
      print('FilePath: $filePath');
      
      showDialog(
        context: context,
        builder: (BuildContext context) => FileUploadDialog(
          url: filePath,
          buttonText: 'Add',
        )
      );
      
      
    //});
  }
  
  // LoginBlock loginBlock;

  void showDemoDialog({BuildContext context, Widget child}) {
    showCupertinoDialog<String>(
      context: context,
      builder: (BuildContext context) => child,
    ).then((String value) {
      if (value != null) {
        setState(() {
          lastSelectedValue = value;
        });
      }
    });
  }

  void showDemoActionSheet({BuildContext context, Widget child}) {
    showCupertinoModalPopup<String>(
      context: context,
      builder: (BuildContext context) => child,
    ).then((String value) {
      if (value != null) {
        setState(() {
          lastSelectedValue = value;
        });
      }
    });
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    _image = image;
    if (image != null) {
      DocumentModel documentModel = DocumentModel(
        file: image.path,
        fileType: "image",
        type: dropdownValue,
        objFile: image,
      );
      Provider.of<LoginBlock>(context).addToDocument(documentModel);
    }
  }

  Future captureImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    if (image != null) {
      DocumentModel documentModel = DocumentModel(
        file: image.path,
        fileType: "image",
        type: dropdownValue,
        objFile: image,
      );

      print('one');
      print(documentModel.toMap());
      Provider.of<LoginBlock>(context).addToDocument(documentModel);
    }
  }

  void _getDocuments() async {
    docPaths = await DocumentsPicker.pickDocuments;

    if (!mounted) return;

    if (docPaths != null) {
      DocumentModel documentModel = DocumentModel(
        file: docPaths[0],
        fileType: "doc",
        type: dropdownValue,
        objFile: File(docPaths[0]),
      );

      print('twgo');

      print(documentModel.toMap());
      Provider.of<LoginBlock>(context).addToDocument(documentModel);
    }
  }

  void uploadFile() {}

  ScrollController _controller = ScrollController();

  double _width = 0.0;
  double _height = 0.0;
  @override
  Widget build(BuildContext context) {
    enableShareReceiving();

    final ui.Size logicalSize = MediaQuery.of(context).size;
    _width = logicalSize.width;
    _height = logicalSize.height;

    var bloc = Provider.of<LoginBlock>(context);

    //List<DocModel> docList = bloc.docList;



    return Stack(
      children: <Widget>[
        
        Column(
          children: <Widget>[
        Container(
          margin: EdgeInsets.all(10),
          
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8.0),
            boxShadow: [
              BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0.0, 2.0),
                  blurRadius: 15.0),
              BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0.0, 2.0),
                  blurRadius: 15.0),
            ],
          ),
          height: 160,
          width: _width - 15,
          //color: Colors.white,
          child: Column(
            //  crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 15),
                child: Container(
                  color: Colors.white,
                  // width: 340,
                  // height: 50,
                  child: Center(
                    child: DropdownButton<String>(
                      hint: Text('     Select Type     '),
                      value: dropdownValue,
                      iconSize: 30,
                      iconEnabledColor: Preferences.appColor,
                      // icon: Icon(Icons.arrow_downward),
                      elevation: 50,
                      style: TextStyle(
                          color: Preferences.appColor, fontSize: 20),
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValue = newValue;
                        });
                      },

                      items: <String>[
                        'Sales',
                        'Purchase',
                        'Expenses',
                      ].map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 0.0, horizontal: 1.0),
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    color: Preferences.appColor,
                    splashColor: Colors.white,
                    onPressed: () {
                      showDemoActionSheet(
                          context: context,
                          child: CupertinoActionSheet(
                            title: Text('Select Type'),
                            actions: <Widget>[
                              CupertinoActionSheetAction(
                                child: Text('Gallery'),
                                onPressed: () {
                                  Navigator.of(context).pop("Discard");
                                  if (dropdownValue == null) {
                                    _showDialog(context, 'Warning!!!',
                                        'Please select type');
                                  } else {
                                    getImage();
                                  }
                                },
                              ),
                              CupertinoActionSheetAction(
                                child: Text('Camera'),
                                onPressed: () {
                                  Navigator.of(context).pop("Discard");
                                  if (dropdownValue == null) {
                                    _showDialog(context, 'Warning!!!',
                                        'Please select type');
                                  } else {
                                    captureImage();
                                  }
                                },
                              ),
                              CupertinoActionSheetAction(
                                child: Text('Document'),
                                onPressed: () {
                                  Navigator.of(context).pop("Discard");
                                  if (dropdownValue == null) {
                                    _showDialog(context, 'Warning!!!',
                                        'Please select type');
                                  } else {
                                    _getDocuments();
                                  }
                                },
                              )
                            ],
                            cancelButton: CupertinoActionSheetAction(
                              child: Text('Cancel'),
                              isDefaultAction: true,
                              onPressed: () {
                                Navigator.pop(context, 'Cancle');
                              },
                            ),
                          ));
                    },
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          'Select File',
                          style: TextStyle(
                            color: Preferences.textColor,
                            fontSize: 18,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        _list(context),
        ],
        ),
        bloc.docIsUpLoading ? _progressHUD : Container(),
      ],
    );
  }





  Widget _list(BuildContext context) {
    List<DocumentModel> documents = Provider.of<LoginBlock>(context).documents;
    print(documents.length);
    return Container(
      color: Colors.white.withOpacity(0.8),
      width: _width - 20.0,
      child: ListView.separated(
          separatorBuilder: (context, index) => Divider(
                color: Colors.black,
              ),
          shrinkWrap: true,
          // scrollDirection: Axis.horizontal,
          controller: _controller,
          itemCount: documents.length,
          itemBuilder: (context, int index) {
            var docModel = documents[index];
            //print(dictDoc);
            String filePath = docModel.file;
            var fileSplit = filePath.split('/');
            var icon = (docModel.fileType == "image")
                ? Icons.image
                : Icons.library_books;
            return _tile(docModel.type, fileSplit.last, icon, index);
          }),
    );
  }

  ListTile _tile(String title, String subtitle, IconData icon, int index) =>
      ListTile(
        title: Text(title,
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 20,
            )),
        subtitle: Text(subtitle),
        leading: Icon(
          icon,
          color: Colors.blue[500],
        ),
        trailing: Container(
          child: SizedBox(
            width: 40,
            child: FlatButton(
              key: ValueKey('$index'),
              child: Icon(Icons.delete),
              onPressed: () {
                print(index);
                setState(() {
                  Provider.of<LoginBlock>(context).removeToDocument(index);
                });

                // List d = [...docList];
                // d.removeAt(index);
                // setState(() {
                //   docList = d;
                // });
              },
            ),
          ),
        ),
      );
}



void _showDialog(BuildContext context, String strTitle, String strMessage) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(strTitle),
          content: Text(strMessage),
          actions: <Widget>[
            new FlatButton(
              child: Text('Close'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      });
}

Future<Null> _submitDialog(BuildContext context) async {
  return await showDialog<Null>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return SimpleDialog(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          children: <Widget>[
            Center(
              child: CircularProgressIndicator(),
            )
          ],
        );
      });
}
