import 'package:fileupload/models/MessageMode.dart';
import 'package:flutter/material.dart';
import 'package:fileupload/models/MessageMode.dart';
import 'dart:ui' as ui;
import 'package:intl/intl.dart';
class MessageCell extends StatelessWidget {
  final List<MessageModel> products;
  MessageCell(this.products);
  
  String returnDateFormate(String strDate) {
    
    var formate = DateFormat('yyyy-MM-dd HH:mm:ss').parse(strDate);
    return DateFormat('dd-MM-yyyy HH:mm:ss').format(formate);
  }

  Widget _buildProductItem(BuildContext context, int index) {
    final ui.Size logicalSize = MediaQuery.of(context).size;
    return Card(
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              width: logicalSize.width - 10.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
                    child: Text('Date: ${returnDateFormate(products[index].msgDate)}',
                      style: TextStyle(color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.bold)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
                    child: Text('${products[index].message}' ,
                        style: TextStyle(color: Colors.black, fontSize: 15.0)),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),

      /*Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          //Image.asset('assets/macbook.jpg'),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(products[index].type, style: TextStyle(color: Colors.black)),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(products[index].doc_name, style: TextStyle(color: Colors.deepPurple)),
          )
          
        ],
      )*/
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: _buildProductItem,
      itemCount: products.length,
    );
  }
}
