import 'package:fileupload/models/CustomDialog.dart';
import 'package:flutter/material.dart';
import 'package:fileupload/models/docModel.dart';
import 'dart:ui' as ui;
import 'package:url_launcher/url_launcher.dart';

class DocumentCell extends StatelessWidget {
  final List<DocModel> products;
  DocumentCell(this.products);

  _launchURL(String docURL) async {
    //const url = docURL;
    if (await canLaunch(docURL)) {
      await launch(docURL);
    } else {
      throw 'Could not launch $docURL';
    }
  }

  Widget _buildProductItem(BuildContext context, int index) {
    final ui.Size logicalSize = MediaQuery.of(context).size;

    return GestureDetector(
      onTap: () {
        //_launchURL();

        (products[index].filetype == 'image') ? 
        showDialog(
          context: context,
          builder: (BuildContext context) => CustomDialog(
            title: '${products[index].type}',
            description: '${products[index].doc_url}',
            buttonText: "Close",
          ),
        ) : _launchURL('${products[index].doc_url}'); 
      },
      child: Card(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              width: logicalSize.width - 90.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0, top: 10.0),
                    child: Text('${products[index].upload_date}',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold)),
                  ),
                  SizedBox(height: 5.0,),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0, bottom: 10.0),
                    child: Text(
                      'File: ${products[index].doc_name}',
                      style: TextStyle(color: Colors.black, fontSize: 15.0),
                      maxLines: 3,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              //color: Colors.red,
              //padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 10.0, top: 0.0),
                    child: Text('${products[index].type}',
                        style: TextStyle(color: Colors.black, fontSize: 15.0)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 10.0, top: 15.0),
                    child: (products[index].status == "0")
                        ? Text('Pending',
                            style: TextStyle(
                                color: Colors.yellow[800], fontSize: 15.0))
                        : (products[index].status == "1")
                            ? Text('Accepted',
                                style: TextStyle(
                                    color: Colors.green, fontSize: 15.0))
                            : Text('Rejected',
                                style:
                                    TextStyle(color: Colors.red, fontSize: 15.0)),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: _buildProductItem,
      itemCount: products.length,
    );
  }
}
