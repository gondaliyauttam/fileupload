import 'package:flutter/material.dart';
import 'package:fileupload/UserDetailsData.dart';
import 'dart:ui' as ui;
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:fileupload/globals.dart';
class ThirdTab extends StatefulWidget {
  @override
  _ThirdTabState createState() => _ThirdTabState();
}

class _ThirdTabState extends State<ThirdTab> {
  var _controller = TextEditingController();
  String dropdownValue;

  @override
  Widget build(BuildContext context) {
    final ui.Size logicalSize = MediaQuery.of(context).size;
    final double _width = logicalSize.width;
    final double _height = logicalSize.height;
    // ScreenUtil.instance = ScreenUtil.getInstance()..init(context);
    // ScreenUtil.instance =
    //     ScreenUtil(width: 500, height: 1600, allowFontScaling: true);

    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8.0),
            boxShadow: [
              BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0.0, 2.0),
                  blurRadius: 15.0),
              BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0.0, 2.0),
                  blurRadius: 15.0),
            ],
          ),
          height: 320,
          width: _width - 15,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Container(
                  color: Colors.white,
                  width: 200,
                  height: 50,
                  child: DropdownButton<String>(
                    hint: Text('      Select Type   '),

                    value: dropdownValue,
                    iconSize: 30,
                    iconEnabledColor: Preferences.appColor,
                    // icon: Icon(Icons.arrow_downward),
                    elevation: 50,
                    style: TextStyle(color: Colors.blue, fontSize: 20),

                    // underline: Container(
                    //   height: 2,
                    //   color: Preferences.appColor,
                    // ),
                    onChanged: (String newValue) {
                      setState(() {
                        dropdownValue = newValue;
                      });
                    },
                    items: <String>['Account', 'Insurance']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 9.0, right: 9.0, top: 9.0),
                child: TextFormField(
                  maxLines: 5,
                  controller: _controller,
                  decoration: InputDecoration(
                      hintText: 'Type Here',
                      fillColor: Colors.white,
                      suffixIcon: IconButton(
                        splashColor: Colors.blueAccent,
                        icon: Icon(Icons.cancel),
                        onPressed: () {
                          _controller.clear();
                        },
                      ),
                      filled: true,
                      border: OutlineInputBorder(),
                      hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0)),
                  
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  color: Preferences.appColor,
                  textColor: Colors.white,
                  splashColor: Colors.white,
                  onPressed: () {
                    if (_controller.text == "") {
                        showAlretBox(context, 'Warning!!!', 'Please enter message.');
                    }else if(dropdownValue == null){
                      showAlretBox(context, 'Warning!!!', 'Please select type');
                      
                    }
                    else
                    {
                      requestMessageAPI(_controller.text, dropdownValue);
                    }
                    
                  },
                  child: Text(
                    'Request',
                    style: TextStyle(fontSize: 20, color: Preferences.textColor),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  void requestMessageAPI(String strmessage, String strType) async {
    UserDetailData userDetailData = UserDetailData();
    userDetailData = await userDetailData.getUserDetail();

    var url = Uri.parse('http://company.depixed.in/api/request-itr-store');
    var request = new http.MultipartRequest('POST', url);
    request.fields['message'] = strmessage;
    request.fields['type'] = (strType == "Account") ? '0' : '1';
    
    request.headers['Accept'] = 'application/json';
    request.headers['Content-Type'] = 'application/json';
    request.headers['Authorization'] = 'Bearer ${userDetailData.userToken}';

    request.send().then((response) async {
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }

      final respStr = await response.stream.bytesToString();

      Map decodedata = json.decode(respStr);
      
      showAlretBox(context, 'Success', decodedata['message']);
      _controller.text = '';
    });

    //Map decodedata = json.decode(response.request);
  }
}
