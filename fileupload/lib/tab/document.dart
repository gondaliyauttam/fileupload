import 'package:fileupload/UserDetailsData.dart';
import 'package:flutter/material.dart';
import 'package:fileupload/globals.dart';
import 'dart:async';
import 'package:fileupload/UserDetailsData.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:fileupload/models/docModel.dart';
import 'dart:ui' as ui;
import 'package:fileupload/tab/DocumentCell.dart';
import 'package:provider/provider.dart';

class SecondTab extends StatefulWidget {
  @override
  _SecondTabState createState() => _SecondTabState();
}

class _SecondTabState extends State<SecondTab> {
  UserDetailData _userDetailData;

  List<DocModel> docList = [];


  @override
  void initState() {
    super.initState();
    //_products.add('Laptop');

    Future.delayed(Duration.zero,(){

      getFiles();
    });
  }


  Future getFiles() async{

    var bloc = Provider.of<LoginBlock>(context);
              bloc.setDocIsLoading(true);

     await bloc.getfiles();
    }

  @override
  Widget build(BuildContext context) {

    var bloc = Provider.of<LoginBlock>(context);

    List<DocModel> docList = bloc.docList;

    return bloc.docIsLoading ? Center(child: CircularProgressIndicator(),): DocumentCell(docList);

  }


  Future<void> documentAPI() async {
    UserDetailData userDetailData = UserDetailData();
    userDetailData = await userDetailData.getUserDetail();

    var url = Uri.parse('http://company.depixed.in/api/document-get');
    var request = new http.MultipartRequest('POST', url);
    request.fields['userID'] = '${userDetailData.userId}';

    request.headers['Accept'] = 'application/json';
    request.headers['Content-Type'] = 'application/json';
    request.headers['Authorization'] = 'Bearer ${userDetailData.userToken}';

    request.send().then((response) async {
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }

      final respStr = await response.stream.bytesToString();

      Map decodedata = json.decode(respStr);
      //print(decodedata);
      List documentList = decodedata['data']['document'];

      docList = [];
      for (var item in documentList) {
        var docObj = DocModel.fromJson(item);
        docList.add(docObj);
        // Provider.of<LoginBlock>(context).docList.add(docObj);
      }

      //   docList = documentList; 
      // });
      // print(documentList);
      // showAlretBox(context, 'Success', decodedata['message']);
      // _controller.text = '';
    });

    //Map decodedata = json.decode(response.request);
  }

  

  
}
