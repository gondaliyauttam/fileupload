import 'dart:io';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'globals.dart';
import 'globals.dart' as global;
import 'UserDetailsData.dart';
import 'package:progress_hud/progress_hud.dart';
class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();
  var _controller = TextEditingController();
  var _controller1 = TextEditingController();
  var _controller2 = TextEditingController();
  var _controller3 = TextEditingController();
  var _controller4 = TextEditingController();
  var _txtPassword = TextEditingController();
  final FocusNode _nameFocus = FocusNode();
  final FocusNode _emailtFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _mobileFocus = FocusNode();
  final FocusNode _companytFocus = FocusNode();
  final FocusNode _addressFocus = FocusNode();
  bool passwordVisible;
  Future<Post> post;

  ProgressHUD _progressHUD;
  bool isShowLoading = false;

  static final Register_URL = "http://company.depixed.in/api/register";

  @override
  void initState() {
    super.initState();
    passwordVisible = true;
    isShowLoading = false;
    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black38,
      color: Colors.white,
      containerColor: Colors.blue,
      borderRadius: 5.0,
      text: 'Loading...',
    );

    Timer.run(() {
      try {
        InternetAddress.lookup('google.com').then((result) {
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            print('connected');
          } else {
            _showDialog(); // show dialog
          }
        }).catchError((error) {
          _showDialog(); // show dialog
        });
      } on SocketException catch (_) {
        _showDialog();
        print('not connected'); // show dialog
      }
    });
    //post = createPost(Register_URL);
  }

  void _showDialog() {
    // dialog implementation
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text("Internet needed!"),
        content: Text("Please connect internet"),
        actions: <Widget>[
          FlatButton(
              child: Text(
                "ok",
                style: TextStyle(color: Preferences.appColor),
              ),
              onPressed: () {
                Navigator.pop(context, true);
              })
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    LoginBlock loginBlock = Provider.of<LoginBlock>(context);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Preferences.appColor,
        title: Text('Register', style: Preferences.textStyleBlack,),
      ),
      // backgroundColor: Colors.blueGrey[100],
      body: Stack(
        children: <Widget>[
          Opacity(
            opacity: 0.6,
            child: Image.asset(
              'assets/images/bg.png',
              fit: BoxFit.fill,
            ),
          ),
          Form(
            key: _formKey,
            child: SafeArea(
              child: ListView(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    textInputAction: TextInputAction.next,
                    controller: _controller,
                    obscureText: false,
                    focusNode: _nameFocus,
                    onFieldSubmitted: (term) {
                      _fieldFocusChange(context, _nameFocus, _emailtFocus);
                    },
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      icon: Icon(Icons.person, size: 15),
                      filled: true,
                      border: OutlineInputBorder(),
                      labelText: 'Person Name',
                      hintText: 'Person Name',
                      suffixIcon: IconButton(
                        splashColor: Colors.blueAccent,
                        icon: Icon(Icons.cancel),
                        onPressed: () {
                          _controller.clear();
                        },
                      ),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please Enter full Name';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next,
                    controller: _controller1,
                    obscureText: false,
                    focusNode: _emailtFocus,
                    onFieldSubmitted: (term) {
                      _fieldFocusChange(context, _emailtFocus, _passwordFocus);
                    },
                    decoration: InputDecoration(
                      icon: Icon(Icons.email, size: 15),
                      fillColor: Colors.white,
                      filled: true,
                      border: OutlineInputBorder(),
                      labelText: 'Email',
                      hintText: 'Email is here',
                      suffixIcon: IconButton(
                        splashColor: Colors.blueAccent,
                        icon: Icon(Icons.cancel),
                        onPressed: () {
                          _controller1.clear();
                        },
                      ),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter Email name';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    textInputAction: TextInputAction.next,
                    controller: _txtPassword,
                    obscureText: passwordVisible,
                    focusNode: _passwordFocus,
                    onFieldSubmitted: (term) {
                      _fieldFocusChange(context, _passwordFocus, _mobileFocus);
                    },
                    decoration: InputDecoration(
                      icon: Icon(Icons.person, size: 15),
                      fillColor: Colors.white,
                      filled: true,
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                      hintText: 'Password',
                      suffixIcon: IconButton(
                        splashColor: Colors.blueAccent,
                        icon: Icon(passwordVisible
                            ? Icons.visibility_off
                            : Icons.visibility),
                        onPressed: () {
                          setState(() {
                            passwordVisible = !passwordVisible;
                          });
                        },
                      ),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter password';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.phone,
                    textInputAction: TextInputAction.next,
                    controller: _controller4,
                    obscureText: false,
                    focusNode: _mobileFocus,
                    onFieldSubmitted: (term) {
                      _fieldFocusChange(context, _mobileFocus, _companytFocus);
                    },
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      icon: Icon(Icons.phone, size: 15),
                      filled: true,
                      border: OutlineInputBorder(),
                      labelText: 'Mobile',
                      hintText: 'Enter mobile number',
                      suffixIcon: IconButton(
                        splashColor: Colors.blueAccent,
                        icon: Icon(Icons.cancel),
                        onPressed: () {
                          _controller4.clear();
                        },
                      ),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please Enter mobile number';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    textInputAction: TextInputAction.next,
                    controller: _controller2,
                    obscureText: false,
                    focusNode: _companytFocus,
                    onFieldSubmitted: (term) {
                      _fieldFocusChange(context, _companytFocus, _addressFocus);
                    },
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      icon: Icon(Icons.business_center, size: 15),
                      filled: true,
                      border: OutlineInputBorder(),
                      labelText: 'Company Name',
                      hintText: 'Company Name is here',
                      suffixIcon: IconButton(
                        splashColor: Colors.blueAccent,
                        icon: Icon(Icons.cancel),
                        onPressed: () {
                          _controller2.clear();
                        },
                      ),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please Enter Company Name';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    textInputAction: TextInputAction.done,
                    controller: _controller3,
                    maxLines: 5,
                    obscureText: false,
                    focusNode: _addressFocus,
                    onFieldSubmitted: (value) {
                      _addressFocus.unfocus();
                    },
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      icon: Icon(Icons.border_color, size: 15),
                      filled: true,
                      border: OutlineInputBorder(),
                      labelText: 'Address',
                      hintText: 'Address is here',
                      suffixIcon: IconButton(
                        splashColor: Colors.blueAccent,
                        icon: Icon(Icons.cancel),
                        onPressed: () {
                          _controller3.clear();
                        },
                      ),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please Enter Address';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 30),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      splashColor: Colors.blue[50],
                      padding: EdgeInsets.all(10),
                      color: Preferences.appColor,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            'Register',
                            style: TextStyle(fontSize: 20, color: Preferences.textColor),
                          ),
                        ],
                        //'Save',
                        //style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          setState(() {
                            isShowLoading = true;
                          });
                          //Scaffold.of(context).showSnackBar(SnackBar(content: Text('Processing Data')));
                          Post newPost = new Post(
                              name: _controller.text,
                              email: _controller1.text,
                              address: _controller3.text,
                              companyName: _controller2.text,
                              pushToken: loginBlock.pushToken,
                              mobile: _controller4.text,
                              password: _txtPassword.text);
                          UserDetailData userDetailData = await createPost(
                              Register_URL,
                              body: newPost.toMap());
                          // print(p.apiMessage);
                          // print(p.success);
                          print(userDetailData.userToken);
                          if (userDetailData.success) {
                            loginBlock.setBoolPreferences(true, loginBlock.isLoginKey);
                            loginBlock.getCompanyName();
                            Navigator.pop(context);
                            // Navigator.push(context,
                            //   MaterialPageRoute(builder: (context) => MyHome())
                            // );
                          }else {
                            showAlretBox(context, 'Warning!!!', '${userDetailData.message}' );//'You have already registered. Please try to LOGIN.');
                          }
                          setState(() {
                            isShowLoading = false;
                          });
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          isShowLoading ? _progressHUD : Container(),
        ],
      ),
    );
  }
}

_fieldFocusChange(
    BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
  currentFocus.unfocus();
  FocusScope.of(context).requestFocus(nextFocus);
}

class Post {
  final String name;
  final String email;
  final String companyName;
  final String address;
  final String mobile;
  final String password;
  final String pushToken;

  Post(
      {this.name,
      this.email,
      this.companyName,
      this.address,
      this.pushToken,
      this.mobile,
      this.password});

  Map toMap() {
    var map = new Map<String, dynamic>();
    map['name'] = name == null ? '' : name;
    map['email'] = email == null ? '' : email;
    map['company'] = companyName == null ? '' : companyName;
    map['address'] = address == null ? '' : address;
    map['device_token'] = pushToken == null ? '' : pushToken;
    map['mobile'] = mobile == null ? '' : mobile;
    map['password'] = password == null ? '' : password;
    return map;
  }
}

Future<UserDetailData> createPost(String url, {Map body}) async {
  return http.post(url, body: body).then((http.Response response) {
    final int statusCode = response.statusCode;

    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }

    Map decodedata = json.decode(response.body);

    print(decodedata);
    if (decodedata != null) {
      var userDetail = UserDetailData.fromJson(decodedata);
      String user = jsonEncode(userDetail); //json.encode();// jsonEncode();
      global.setStringPreferences(user, SharePrefName.userDetails);
    }

    return UserDetailData.fromJson(json.decode(response.body));
  });
}
