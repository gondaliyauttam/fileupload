import 'dart:convert';
import 'globals.dart' as global;

class UserDetailData {
  final bool success;
  final String userToken;
  final String userName;
  final int userId;
  final String userAddress;
  final String userEmail;
  final String notification;
  final String company;
  String message = '';

  UserDetailData(
      {this.success,
      this.userAddress,
      this.userEmail,
      this.userId,
      this.userName,
      this.userToken,
      this.notification,
      this.message,
      this.company});

  factory UserDetailData.fromJson(Map<String, dynamic> json) {
    var suc =
        (json['success'] is bool) ? json['success'] : json['success'] == 'true';
    if (suc) {
      var userDetail = UserDetailData(
        success: (json['success'] is bool)
            ? json['success']
            : json['success'] == 'true',
        //apiMessage: json['msg'] == null ? "" : json['msg'],
        userToken: json['data']['token'],
        userName: json['data']['name'],
        userId: json['data']['id'],
        userAddress: json['data']['address'],
        userEmail: json['data']['email'],
        notification: json['data']['notification'],
        company: json['data']['company'],
      );
      return userDetail;
    }else {
      var userDetail = UserDetailData(
        success: (json['success'] is bool)
            ? json['success']
            : json['success'] == 'true',
        message: json['message'],
      );
      return userDetail;
    }
  }

  Map<String, dynamic> toJson() => {
        'success': success,
        'data': data(),
      };

  Map<String, dynamic> data() => {
        'token': userToken,
        'name': userName,
        'id': userId,
        'address': userAddress,
        'email': userEmail,
        'notification': notification,
        'company' : company,
      };

  Future<UserDetailData> getUserDetail() async {
    var strUserDetails =
        await global.getStringPreferences(global.SharePrefName.userDetails);
    // var strUserDetail = value;
    Map userMap = jsonDecode(strUserDetails);
    UserDetailData userDetailData = UserDetailData.fromJson(userMap);
    print('getUserDetail $userDetailData');
    return userDetailData;
    //UserDetailData userDetailData = strUserDetail
    //userToken = value;
  }
}
