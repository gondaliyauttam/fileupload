import 'dart:io';
import 'package:path/path.dart' as p;
import 'package:fileupload/models/document.model.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:fileupload/globals.dart';
import 'package:provider/provider.dart';

class FileUploadDialog extends StatefulWidget {
  final String url;
  final Image image;
  final String buttonText;
  double _width = 0.0;
  double _height = 0.0;

  FileUploadDialog({
    @required this.url,
    @required this.buttonText,
    this.image,
  });

  @override
  FileUploadDialogState createState() => FileUploadDialogState();
}

class FileUploadDialogState extends State<FileUploadDialog> {
  String dropdownValue;

  @override
  Widget build(BuildContext context) {
    final ui.Size logicalSize = MediaQuery.of(context).size;
    widget._width = logicalSize.width;
    widget._height = logicalSize.height;
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Consts.padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height: 200.0,
          padding: EdgeInsets.only(
            top: Consts.avatarRadius + Consts.padding,
            bottom: Consts.padding,
            left: Consts.padding,
            right: Consts.padding,
          ),
          margin: EdgeInsets.only(
              top: Consts.avatarRadius, bottom: Consts.avatarRadius),
          decoration: new BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(Consts.padding),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.max, // To make the card compact
            children: <Widget>[
              DropdownButton<String>(
                hint: Text('      Select Type   '),

                value: dropdownValue,
                iconSize: 30,
                iconEnabledColor: Preferences.appColor,
                // icon: Icon(Icons.arrow_downward),
                elevation: 50,
                style: TextStyle(color: Colors.blue, fontSize: 20),

                onChanged: (String newValue) {
                  setState(() {
                    dropdownValue = newValue;
                  });
                },
                items: <String>['Sales', 'Purchase', 'Expenses']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
              SizedBox(height: 10.0),
              Container(
                child: Text('URL: ${widget.url}'),
                // WebView(
                //   initialUrl: description,
                // )
              ),
              SizedBox(height: 15.0),
              Align(
                alignment: Alignment.bottomRight,
                child: FlatButton(
                  color: Colors.tealAccent[400],
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15)),
                  onPressed: () {
                    
                    if (dropdownValue == null){
                      showAlretBox(context, 'Warning!!!', 'Please select type');
                    }else {
                      var objFile = File(widget.url);
                      var extension = p.extension(objFile.path);
                      var filetype;
                      if(extension == '.jpg' || extension == '.png' || extension == '.jpeg'){
                        filetype = 'image';
                      }else {
                          filetype = 'doc';
                      }
                      DocumentModel documentModel = DocumentModel(
                        file: widget.url,
                        fileType: filetype,
                        type: dropdownValue,
                        objFile: objFile);
                      Provider.of<LoginBlock>(context)
                        .addToDocument(documentModel);
                      Navigator.of(context).pop();
                    } // To close the dialog
                    
                  },
                  child: Text(widget.buttonText),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class Consts {
  Consts._();

  static const double padding = 5.0;
  static const double avatarRadius = 5.0;
}
