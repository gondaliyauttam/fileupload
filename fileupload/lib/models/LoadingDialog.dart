import 'package:flutter/material.dart';

class LoadingDialog extends StatefulWidget{
  LoadingDialogState state;

  bool isShowing(){
    return state!=null&&state.mounted;
  }

  @override
   createState()=> state=LoadingDialogState();
}

class LoadingDialogState extends State<LoadingDialog>{

  LoadingDialog loadingDialog;

  @override
  Widget build(BuildContext context) {
    return  Align(alignment: Alignment.center,
      child:  CircularProgressIndicator(valueColor: AlwaysStoppedAnimation(Colors.lightBlue
      ) ,),);
  }

  void showLoadingDialog() async {
    await new Future.delayed(new Duration(milliseconds: 30));
    await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) =>
        loadingDialog = loadingDialog ?? LoadingDialog());
    //builder: (BuildContext context) => WillPopScope(child:loadingDialog = LoadingDialog() , onWillPop:  () async => false,));
  }
  Future hideDialog() {
    if (loadingDialog != null && loadingDialog.isShowing()) {
      Navigator.of(context).pop();
      loadingDialog = null;
    }
  }


}