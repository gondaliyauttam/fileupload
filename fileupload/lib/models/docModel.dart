

class DocModel {
  final int docid;
  final String type;
  final String status;
  final String upload_date;
  final String doc_url;
  final String doc_name;
  final String filetype;

  DocModel({this.docid,this.type,this.status,this.upload_date,this.doc_name,this.doc_url,this.filetype});

  factory DocModel.fromJson(Map<String, dynamic> json){

     var docDetail =  DocModel(
      
      docid: json['documentsID'],
      type: json['type'],
      status: json['status'],
      upload_date: json['uploaded_date'],
      doc_url: json['doc_url'],
      doc_name: json['doc_name'],
      filetype: json['file_type'],
    );
    return docDetail;

  }
}