import 'package:intl/intl.dart';

class MessageModel {
  final int msgId;
  final String msgDate;
  final String message;
  

  MessageModel({this.msgId,this.msgDate,this.message});

  factory MessageModel.fromJson(Map<String, dynamic> json){

     var docDetail =  MessageModel(
      msgId: json['id'],
      msgDate: json['created_at'],
      message: json['message'],
    );
    return docDetail;

  }

  
}