import 'dart:io';

import 'package:flutter/material.dart';

class DocumentModel {
  File objFile;
  String file;
  String fileType;
  String type;

  DocumentModel({@required this.file, @required this.fileType, @required this.type, @required this.objFile});


  toMap()
  {
    var map = new Map();
    map['objFile'] = this.objFile;
    map['file'] = this.file;
    map['fileType'] = this.fileType;
    map['type'] = this.type;

    return map;
  }

}
