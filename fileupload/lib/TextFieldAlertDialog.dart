import 'package:fileupload/globals.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
class CustomAlertDialog extends StatefulWidget {
  final String title;
  //final FirebaseAuth auth;

  const CustomAlertDialog({Key key, this.title})
      : super(key: key);

  @override
  CustomAlertDialogState createState() {
    return new CustomAlertDialogState();
  }
}

class CustomAlertDialogState extends State<CustomAlertDialog> {

  final _resetKey = GlobalKey<FormState>();
  final _resetEmailController = TextEditingController();
  String _resetEmail;
  bool _resetValidate = false;

  // StreamController<bool> rebuild = StreamController<bool>();

  Future<bool> _sendResetEmail() async {
    _resetEmail = _resetEmailController.text;

    if (_resetKey.currentState.validate()) {
      _resetKey.currentState.save();

      try {
        // You could consider using async/await here
        //widget.auth.sendPasswordResetEmail(email: _resetEmail);
        Map resultdata = await forgotPasswordAPICall('http://company.depixed.in/send-reset-password-link',body: {'email':_resetEmail});
        if (resultdata['success']) {
          Navigator.of(context).pop(_resetEmail);
          showAlretBox(context, 'Success', resultdata['message']);
        } else {
          showAlretBox(context, 'Warning!!!', resultdata['message']);
        }
        return true;
      } catch (exception) {
        print(exception);
      }
    } else {
      setState(() {
        _resetValidate = true;
      });
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: AlertDialog(
        title: new Text(widget.title),
        content: new SingleChildScrollView(
            child: Form(
              key: _resetKey,
              autovalidate: _resetValidate,
              child: ListBody(
                children: <Widget>[
                  new Text(
                    'Enter the Email Address associated with your account.',
                    style: TextStyle(fontSize: 14.0),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10.0),
                  ),
                  Row(
                    children: <Widget>[
                      new Expanded(
                        child: TextFormField(
                          validator: validateEmail,
                          onSaved: (String val) {
                            _resetEmail = val;
                          },
                          controller: _resetEmailController,
                          keyboardType: TextInputType.emailAddress,
                          autofocus: true,
                          decoration: new InputDecoration(
                      fillColor: Colors.white,
                      //icon: Icon(Icons.email, size: 25),
                      filled: true,
                      border: OutlineInputBorder(),
                      labelText: 'Email',
                      hintText: 'Enter email id.',
                      suffixIcon: IconButton(
                        splashColor: Colors.blueAccent,
                        icon: Icon(Icons.cancel),
                        onPressed: () {
                          _resetEmailController.clear();
                        },
                      ),
                    ),
                          style: TextStyle(color: Colors.black),

                        ),
                      )
                    ],
                  ),
                  new Column(children: <Widget>[
                    Container(
                      decoration: new BoxDecoration(
                          border: new Border(
                              bottom: new BorderSide(
                                  width: 0.5, color: Colors.black))),
                    )
                  ]),
                ],
              ),
            ),
        ),
        actions: <Widget>[
          new FlatButton(
            child: new Text(
              'CANCEL',
              style: TextStyle(color: Colors.black),
            ),
            onPressed: () {
              Navigator.of(context).pop("");
            },
          ),
          new FlatButton(
            child: new Text(
              'SEND EMAIL',
              style: TextStyle(color: Colors.black),
            ),
            onPressed: () {
              LoginBlock loginBlock = Provider.of<LoginBlock>(context);
              loginBlock.setIsLoadingForgot(true);
              _sendResetEmail();
              // if (_sendResetEmail()) {
              //   Navigator.of(context).pop(_resetEmail);
              // }
            },
          ),
        ],
      ),
    );
  }
}

String validateEmail(String value) {
  String pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regExp = new RegExp(pattern);
  if (value.length == 0) {
    return "Email is required";
  } else if (!regExp.hasMatch(value)) {
    return "Invalid Email";
  } else {
    return null;
  }
}


Future<Map> forgotPasswordAPICall(String url, {Map body}) async {
  return http.post(url, body: body).then((http.Response response) {
    final int statusCode = response.statusCode;

    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }

    Map decodedata = json.decode(response.body);

    

    return decodedata;
  });
}