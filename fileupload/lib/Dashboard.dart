import 'package:fileupload/register.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'Login.dart';
import 'package:provider/provider.dart';
import 'globals.dart';
class Dashboard extends StatefulWidget{

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    LoginBlock loginBlock = Provider.of<LoginBlock>(context);
    return Scaffold(
      //margin: EdgeInsets.all(0.0),
      //color: Colors.red,
      body: ModalProgressHUD(
        inAsyncCall: loginBlock.isLoadingForgot,
        child: Stack(
          children: <Widget>[
            Opacity(
              opacity: 0.6,
              child: Image.asset(
                'assets/images/bg.png',
                fit: BoxFit.fill,
              ),
            ),
            Center(
              child: Column(
                
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 300,
                    //color: Colors.black,
                    //padding: EdgeInsets.all(100),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          splashColor: Preferences.appColor,
                          padding: EdgeInsets.all(10),
                          color: Preferences.appColor,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                'Login',
                                style: TextStyle(fontSize: 20, color: Preferences.textColor),
                              ),
                              
                            ],
                            //'Save',
                            //style: TextStyle(color: Colors.white, fontSize: 20),
                          ), 
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LoginPage(),
                              ),
                            );
                          },
                    ),
                  ),
                  SizedBox(height: 20,),
                  Container(
                    width: 300,
                    
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          splashColor: Colors.blue[50],
                          padding: EdgeInsets.all(10),
                          color: Preferences.appColor,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                'Register',
                                style: TextStyle(fontSize: 20, color: Preferences.textColor),
                              ),
                              
                            ],
                            //'Save',
                            //style: TextStyle(color: Colors.white, fontSize: 20),
                          ), 
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => RegisterPage(),
                              ),
                            );
                          },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

}