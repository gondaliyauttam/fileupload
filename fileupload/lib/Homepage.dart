import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fileupload/tab/document.dart';
import 'tab/home.dart';
import 'package:fileupload/tab/request.dart';
import 'package:fileupload/globals.dart';
import 'package:provider/provider.dart';
import 'tab/message.dart';
import 'package:flutter/services.dart';

class MyHome extends StatefulWidget {
  MyHome({Key key}) : super(key: key);
  @override
  MyHomeState createState() => MyHomeState();
}

class MyHomeState extends State<MyHome> with SingleTickerProviderStateMixin {
  //TabController controller;
  static const platform = const MethodChannel('app.channel.shared.data');
  String dataShared = "No data";

  final List<MyTabs> _tabs = [
    new MyTabs(title: "Home", color: Preferences.textColor),
    new MyTabs(title: "Document", color: Preferences.textColor),
    new MyTabs(title: "Request", color: Preferences.textColor)
  ];
  MyTabs _myHandler;
  TabController _controller;
  //var _isVisible = false;

  @override
  void initState() {
    super.initState();
    _controller = new TabController(length: 3, vsync: this);
    _myHandler = _tabs[0];
    _controller.addListener(_handleSelected);

    Future.delayed(Duration.zero,(){
      Provider.of<LoginBlock>(context).getMessageCouner();
      if (Provider.of<LoginBlock>(context).isLogin) {
        Provider.of<LoginBlock>(context).getCompanyName();
      }
    });
  }

  void _handleSelected() {
    setState(() {
      _myHandler = _tabs[_controller.index];
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var bloc = Provider.of<LoginBlock>(context);
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: new Text(_myHandler.title == "Home" ? bloc.companyName : _myHandler.title, style: Preferences.textStyleBlack,),
        centerTitle: true,
        backgroundColor: Preferences.appColor,//Preferences.appColor,
        leading: IconButton(
                icon: Stack(
                  children: <Widget>[
                    new Image.asset('assets/images/logout.png', width: 25.0,)
                  ],
                ), 
                onPressed: () {
                  showLogoutDialog(context);
                },
              ),
        actions: <Widget>[
          Row(
            children: <Widget>[
              
              IconButton(
                icon: new Stack(
                  children: <Widget>[
                    new Icon(Icons.message,),
                    new Positioned(
                      right: 0,
                      child: (bloc.unReadMsg == '0' || bloc.unReadMsg == "") ? Container() : new Container(
                        padding: EdgeInsets.all(1),
                        decoration: new BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(6),
                        ),
                        constraints: BoxConstraints(
                          minWidth: 12,
                          minHeight: 12,
                        ),
                        child: new Text(
                           bloc.unReadMsg,
                          style: new TextStyle(
                            color: Colors.white,
                            fontSize: 8,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  ],
                ),
                onPressed: () {
                  debugPrint('TapNotification');
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => MessageScreen(),
                        fullscreenDialog: true),
                  );
                },
              ),
            ],
          ),
        ],
      ),
      floatingActionButton: Visibility(
        visible: (_myHandler.title == "Request")
            ? false
            : (_myHandler.title == "Home")
                ? Provider.of<LoginBlock>(context).isShowUploadButton
                : true,
        child: FloatingActionButton(
          backgroundColor: Preferences.appColor,
          onPressed: () {
            if (_myHandler.title == 'Home') {
              if (bloc.docIsUpLoading) {
                
              } else {
                bloc.setDocIsUpLoading(true);
                bloc.uploadDocument();
              }
              
            } else {
              bloc.setDocIsLoading(true);
              bloc.getfiles();
            }
            // (_myHandler.title=="Home") ? FirstTab().method() : FirstTab().method();
          },

          child: Icon(
            (_myHandler.title == "Home") ? Icons.file_upload : Icons.sync,
            size: 50,
          ),

          //backgroundColor: Preferences.appColor,
        ),
      ),
      body: Stack(
        children: <Widget>[
          new Container(
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("assets/images/bg.png"),
                fit: BoxFit.fill,
              ),
            ),
          ),
          // Opacity(
          //     opacity: 1.0,
          //     child: Image.asset('assets/images/bg.png',fit: BoxFit.fill ,),
          //   ),
          TabBarView(
            children: <Widget>[FirstTab(), SecondTab(), ThirdTab()],
            controller: _controller,
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        color: Preferences.appColor,
        child: Container(
          height: 56,
          child: TabBar(
            labelColor: Preferences.textColor,
            tabs: <Widget>[
              Tab(
                icon: Icon(Icons.home, size: 25),
                
                text: _tabs[0].title,
              ),
              Tab(
                icon: Icon(Icons.attachment, size: 25),
                text: _tabs[1].title,
              ),
              Tab(
                icon: Icon(Icons.format_list_bulleted, size: 25),
                text: _tabs[2].title,
              ),
            ],
            controller: _controller,
          ),
        ),
      ),
    );
  }
}

class MyTabs {
  final String title;
  final Color color;
  MyTabs({this.title, this.color});
}

void showLogoutDialog(BuildContext context) {
  showDialog(
    context: context,
    builder: (BuildContext context){
      return AlertDialog(
        title: Text('Are you sure you want to logout?'),
        actions: <Widget>[
          new FlatButton(
              child: Text('Yes'),
              onPressed: () {
                var loginBlock = Provider.of<LoginBlock>(context);
                loginBlock.setBoolPreferences(false, loginBlock.isLoginKey);
                loginBlock.isLogin = false;
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )

        ],
      );
    } 
  );
}