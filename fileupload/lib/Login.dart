import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'globals.dart';
import 'globals.dart' as global;
import 'UserDetailsData.dart';
import 'package:progress_hud/progress_hud.dart';
import 'dart:ui' as ui;
import 'TextFieldAlertDialog.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  var _txtEmail = TextEditingController(text: '');
  var _txtPassword = TextEditingController(text: '');
  final FocusNode _emailtFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  bool passwordVisible;
  Future<LoginData> post;

  ProgressHUD _progressHUD;
  bool isShowLoading = false;
  double _width = 0.0;
  static final Login_URL = "http://company.depixed.in/api/login";

  @override
  void initState() {
    super.initState();
    passwordVisible = true;
    isShowLoading = false;
    _progressHUD = new ProgressHUD(
      backgroundColor: Colors.black38,
      color: Colors.white,
      containerColor: Colors.blue,
      borderRadius: 5.0,
      text: 'Loading...',
    );
  }

  @override
  Widget build(BuildContext context) {
    LoginBlock loginBlock = Provider.of<LoginBlock>(context);
    final ui.Size logicalSize = MediaQuery.of(context).size;
    _width = logicalSize.width;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Preferences.appColor,
        title: Text('Login', style: Preferences.textStyleBlack,),
      ),
      // backgroundColor: Colors.blueGrey[100],
      body: Stack(
        children: <Widget>[
          Opacity(
            opacity: 0.6,
            child: Image.asset(
              'assets/images/bg.png',
              fit: BoxFit.fill,
            ),
          ),
          Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: ListView(
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next,
                    controller: _txtEmail,
                    obscureText: false,
                    focusNode: _emailtFocus,
                    onFieldSubmitted: (term) {
                      _fieldFocusChange(context, _emailtFocus, _passwordFocus);
                    },
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      icon: Icon(Icons.email, size: 25),
                      filled: true,
                      border: OutlineInputBorder(),
                      labelText: 'Email',
                      hintText: 'Enter email id.',
                      suffixIcon: IconButton(
                        splashColor: Colors.blueAccent,
                        icon: Icon(Icons.cancel),
                        onPressed: () {
                          _txtEmail.clear();
                        },
                      ),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter register email id.';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    textInputAction: TextInputAction.next,
                    controller: _txtPassword,
                    obscureText: passwordVisible,
                    focusNode: _passwordFocus,
                    onFieldSubmitted: (term) {
                      _passwordFocus.unfocus();
                    },
                    decoration: InputDecoration(
                      icon: Icon(Icons.perm_identity, size: 25),
                      fillColor: Colors.white,
                      filled: true,
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                      hintText: 'Password',
                      suffixIcon: IconButton(
                        splashColor: Colors.blueAccent,
                        icon: Icon(passwordVisible
                            ? Icons.visibility_off
                            : Icons.visibility),
                        onPressed: () {
                          setState(() {
                            passwordVisible = !passwordVisible;
                          });
                        },
                      ),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter password';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    alignment: Alignment.bottomRight,
                    child: GestureDetector(
                      child: Text('Forgot Password?',
                          style: TextStyle(
                              decoration: TextDecoration.underline,
                              color: Colors.blue,
                              fontSize: 16.0)),
                      onTap: () {
                        showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) =>
                                CustomAlertDialog(title: "Reset email",));
                      },
                    ),
                  ),
                  SizedBox(
                    height: 40.0,
                  ),
                    Container(
                      width: 200.0,
                      margin: EdgeInsets.only(left: 30),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        splashColor: Colors.blue[50],
                        padding: EdgeInsets.all(10),
                        color: Preferences.appColor,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              'Login',
                              style: TextStyle(fontSize: 20, color: Preferences.textColor),
                            ),
                          ],
                          //'Save',
                          //style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        onPressed: () async {
                          if (_formKey.currentState.validate()) {
                            setState(() {
                              isShowLoading = true;
                            });
                            //Scaffold.of(context).showSnackBar(SnackBar(content: Text('Processing Data')));
                            LoginData newPost = new LoginData(
                                email: _txtEmail.text,
                                password: _txtPassword.text,
                                pushToken: loginBlock.pushToken);
                            UserDetailData userDetailData = await createPost(
                                Login_URL,
                                body: newPost.toMap());
                            print('UserDetail: $userDetailData');
                            if (userDetailData.success) {
                              loginBlock.getCompanyName();
                              loginBlock.setBoolPreferences(
                                  true, loginBlock.isLoginKey);
                              Navigator.pop(context);
                            } else {
                              showAlretBox(
                                  context, 'Warning!!!', userDetailData.message);
                            }
                            setState(() {
                              isShowLoading = false;
                            });
                          }
                        },
                      ),
                    ),
                ],
              ),
            ),
          ),
          isShowLoading ? _progressHUD : Container(),
        ],
      ),
    );
  }
}

_fieldFocusChange(
    BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
  currentFocus.unfocus();
  FocusScope.of(context).requestFocus(nextFocus);
}

class LoginData {
  final String email;
  final String password;
  final String pushToken;
  LoginData({this.email, this.password, this.pushToken});

  Map toMap() {
    var map = new Map<String, dynamic>();
    map['email'] = email == null ? '' : email;
    map['password'] = password == null ? '' : password;
    map['push_token'] = pushToken == null ? '' : pushToken;
    return map;
  }
}
//push_token
Future<UserDetailData> createPost(String url, {Map body}) async {
  return http.post(url, body: body).then((http.Response response) {
    final int statusCode = response.statusCode;

    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }

    Map decodedata = json.decode(response.body);

    print(decodedata);
    if (decodedata != null) {
      if (decodedata['success']) {
        var userDetail = UserDetailData.fromJson(decodedata);
        String user = jsonEncode(userDetail); //json.encode();// jsonEncode();
        global.setStringPreferences(user, SharePrefName.userDetails);
      }
    }

    return UserDetailData.fromJson(json.decode(response.body));
  });
}
