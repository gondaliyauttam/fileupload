import 'package:fileupload/models/document.model.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'UserDetailsData.dart';
import 'dart:convert';
import 'package:http_parser/http_parser.dart';
import 'models/docModel.dart';
import 'models/MessageMode.dart';
//library fileupload.globals;

class Preferences {

  Preferences._();
  static const Color appColor = Color.fromRGBO(26, 152, 115, 1.0);
  static const Color textColor = Colors.black;

  static const TextStyle textStyleBlack = TextStyle(
    color: textColor,
  );
}

class LoginBlock extends ChangeNotifier {
  bool _isLogin = false;
  bool get isLogin => _isLogin;
  String isLoginKey = 'isLogin';
  String callUploadfun;
  String pushToken;
  String connectionStatus;
  bool isConnected = false;
  bool isNotconnected = false;

  bool _isShowUploadButton = false;
  bool get isShowUploadButton => (_documents.length == 0) ? false : true;

  set isShowUploadButton(bool value) {
    _isShowUploadButton = value;
    notifyListeners();
  }

  set isLogin(bool value) {
    _isLogin = value;
    notifyListeners();
  }

  void callUpload() {
    callUploadfun = 'DocumentUpload';
    notifyListeners();
  }

  Future tryLoginWithPreferences() async {
    bool val = await getBoolPreferences(isLoginKey);
    if (val != null) {
      _isLogin = val;
      if (_isLogin) {
        notifyListeners();
      }
      
    }
  }

  

  Future<bool> getBoolPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(key);
  }

  void setBoolPreferences(bool value, String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(key, value);
    notifyListeners();
  }

  List<DocModel> _docList = [];
  List<DocModel> get docList => _docList;

  bool docIsLoading = false;

  setDocIsLoading(bool status) {
    docIsLoading = status;
    notifyListeners();
  }

  bool docIsUpLoading = false;

  setDocIsUpLoading(bool status) {
    docIsUpLoading = status;
    notifyListeners();
  }

  bool isLoadingForgot = false;

  setIsLoadingForgot(bool status) {
    isLoadingForgot = status;
    notifyListeners();
  }

  set docList(List<DocModel> value) {
    _docList = value;
    notifyListeners();
  }

  List<DocumentModel> _documents = [];

  List<DocumentModel> get documents => _documents;

  addToDocument(DocumentModel documentModel) {
    _documents.add(documentModel);
    _isShowUploadButton = (_documents.length == 0) ? false : true;
    notifyListeners();
  }

  removeToDocument(int index) {
    _documents.removeAt(index);
    _isShowUploadButton = (_documents.length == 0) ? false : true;
    notifyListeners();
  }

  int numberOfDocUpload = 0;
  uploadDocument() {
    print(_documents);
    numberOfDocUpload = 0;
    for (var objDocument in _documents) {
      uploadFileAPI(objDocument);
    }
  }

  void uploadFileAPI(DocumentModel documentModel) async {
    UserDetailData userDetailData = UserDetailData();
    userDetailData = await userDetailData.getUserDetail();
    print(userDetailData.userId);
    var file = documentModel.objFile; //Uri.directory(documentModel.file);
    String filePath = documentModel.file;
    var fileName = filePath.split('/').last;
    var url = Uri.parse('http://company.depixed.in/api/document-upload');
    var request = new http.MultipartRequest('POST', url);
    request.fields['file_type'] = documentModel.fileType;
    request.fields['type'] = documentModel.type;
    request.fields['userID'] = '${userDetailData.userId}';
    request.headers['Accept'] = 'application/json';
    request.headers['Content-Type'] = 'multipart/form-data';
    request.headers['Authorization'] = 'Bearer ${userDetailData.userToken}';

    var bytes = file.readAsBytesSync();
    //var bytes = File(documentModel.file).readAsBytesSync();
    //request.files.add(new http.MultipartFile.fromBytes('document', await File.fromUri(fileURL).readAsBytes()), ContentType);
    request.files.add(new http.MultipartFile.fromBytes('document', bytes,
        filename: fileName, contentType: new MediaType('image', 'jpeg')));
    request.send().then((response) async {
      numberOfDocUpload = numberOfDocUpload + 1;
      if (numberOfDocUpload == _documents.length) {
        _documents = [];
        docIsUpLoading = false;
        notifyListeners();
      }
      final int statusCode = response.statusCode;
      final respStr = await response.stream.bytesToString();
      print(respStr);
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }
    });
  }

  Future getfiles() async {
    UserDetailData userDetailData = UserDetailData();
    userDetailData = await userDetailData.getUserDetail();

    var url = Uri.parse('http://company.depixed.in/api/document-get');
    var request = new http.MultipartRequest('POST', url);
    request.fields['userID'] = '${userDetailData.userId}';

    request.headers['Accept'] = 'application/json';
    request.headers['Content-Type'] = 'application/json';
    request.headers['Authorization'] = 'Bearer ${userDetailData.userToken}';

    var response = await request.send();

    final int statusCode = response.statusCode;

    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }

    final respStr = await response.stream.bytesToString();

    Map decodedata = json.decode(respStr);
    //print(decodedata);
    List documentList = decodedata['data']['document'];

    docList = [];
    for (var item in documentList) {
      var docObj = DocModel.fromJson(item);
      docList.add(docObj);
      // Provider.of<LoginBlock>(context).docList.add(docObj);
    }

    docIsLoading = false;

    notifyListeners();
  }

  List<MessageModel> _msgList = [];
  List<MessageModel> get msgList => _msgList;

  set msgList(List<MessageModel> value) {
    _msgList = value;
    notifyListeners();
  }
  String companyName = ''; 
  Future getCompanyName() async {
    UserDetailData userDetailData = UserDetailData();
    userDetailData = await userDetailData.getUserDetail();
    companyName = userDetailData.company;
    notifyListeners();

  }

  Future getMessage() async {
    UserDetailData userDetailData = UserDetailData();
    userDetailData = await userDetailData.getUserDetail();

    var url = Uri.parse('http://company.depixed.in/api/user-messages');
    var request = new http.MultipartRequest('GET', url);

    request.headers['Accept'] = 'application/json';
    request.headers['Content-Type'] = 'application/json';
    request.headers['Authorization'] = 'Bearer ${userDetailData.userToken}';

    var response = await request.send();

    final int statusCode = response.statusCode;

    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }

    final respStr = await response.stream.bytesToString();

    Map decodedata = json.decode(respStr);
    //print(decodedata);
    List messageList = decodedata['messages'];

    msgList = [];
    for (var item in messageList) {
      var docObj = MessageModel.fromJson(item);
      msgList.add(docObj);
      // Provider.of<LoginBlock>(context).docList.add(docObj);
    }
    msgList = msgList.reversed.toList();
    //docIsLoading = false;

    notifyListeners();
  }

  String _unReadMsg = '0';
  String get unReadMsg => _unReadMsg;

  set unReadMsg(String value) {
    _unReadMsg = value;
    notifyListeners();
  }

  Future getMessageCouner() async {
    
    UserDetailData userDetailData = UserDetailData();
    userDetailData = await userDetailData.getUserDetail();

    var url = Uri.parse('http://company.depixed.in/api/user-unread-messages');
    var request = new http.MultipartRequest('GET', url);

    request.headers['Accept'] = 'application/json';
    request.headers['Content-Type'] = 'application/json';
    request.headers['Authorization'] = 'Bearer ${userDetailData.userToken}';

    var response = await request.send();

    final int statusCode = response.statusCode;

    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }

    final respStr = await response.stream.bytesToString();

    Map decodedata = json.decode(respStr);
    //print(decodedata);
    var messageCounter = decodedata['message_count'];
    print('Message Counter: $messageCounter');
    unReadMsg = '$messageCounter';
    notifyListeners();
  }

  Future readAllMessage() async {
    UserDetailData userDetailData = UserDetailData();
    userDetailData = await userDetailData.getUserDetail();

    var url = Uri.parse('http://company.depixed.in/api/change-read-status');
    var request = new http.MultipartRequest('POST', url);
    request.fields['id'] = '${userDetailData.userId}';

    request.headers['Accept'] = 'application/json';
    request.headers['Content-Type'] = 'application/json';
    request.headers['Authorization'] = 'Bearer ${userDetailData.userToken}';

    var response = await request.send();

    final int statusCode = response.statusCode;

    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }

    final respStr = await response.stream.bytesToString();

    Map decodedata = json.decode(respStr);
    print(decodedata);
    
    notifyListeners();
  }

}



void setStringPreferences(String value, String key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString(key, value);
}

Future<String> getStringPreferences(String key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString(key);
}

void showAlretBox(BuildContext context, String strTitle, String strMessage) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(strTitle),
          content: Text(strMessage),
          actions: <Widget>[
            new FlatButton(
              child: Text('Close'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      });
}

class SharePrefName {
  static String userDetails = 'userdetails';
}

class TimeNotification extends Notification {
  final String time;

  const TimeNotification({this.time});
}
