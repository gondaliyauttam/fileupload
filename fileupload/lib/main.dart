import 'package:fileupload/Dashboard.dart';
import 'package:flutter/material.dart';
import 'globals.dart';
import 'Dashboard.dart';
import 'Homepage.dart';
import 'package:provider/provider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:async';
import 'dart:io';

final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

void main() => runApp(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(
            builder: (_) => LoginBlock(),
          )
        ],
        child: MyApp(),
      ),
    );

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var _connectionStatus = 'Unknown';

  Connectivity connectivity;

  StreamSubscription<ConnectivityResult> subscription;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _firebaseMessaging.requestNotificationPermissions();
  }

  @override
  Widget build(BuildContext context) {
    
    
    //Connectivity Check
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      Provider.of<LoginBlock>(context).connectionStatus = result.toString();
      print('connectionStatus : $_connectionStatus');

      try {
        InternetAddress.lookup('google.com').then((result) {
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            print('connected');
            Provider.of<LoginBlock>(context).isConnected = true;
          } else {
            print('not connected');
            Provider.of<LoginBlock>(context).isConnected = false;
          }
        });
      } on SocketException catch (_) {
        print('not connected');
        Provider.of<LoginBlock>(context).isConnected = false;
      }
    });

    callPushNotification(context);
    // Provider.of<LoginBlock>(context).setBoolPreferences(false, 'isLogin');
    //final LoginBlock loginBlock = Provider.of<LoginBlock>(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Provider.of<LoginBlock>(context).isLogin
          ? MyHome()
          : FutureBuilder(
              future:
                  Provider.of<LoginBlock>(context).tryLoginWithPreferences(),
              builder: (context, authResultSnapshot) {
                if (authResultSnapshot.connectionState ==
                    ConnectionState.waiting) {
                  return Container(color: Colors.white,
                      child: Center(child: CircularProgressIndicator()));
                }
                return MainTemplate(
                  title: 'Dashboard',
                  child: Dashboard(),
                );
              },
            ),
    );
    // return globals.getBoolPreferences(globals.isLogin) != null ? MyHome() : new MainTemplate(
    //   title: 'Register',
    //   child: LoginPage(),
    // );
  }

  void callPushNotification(BuildContext context) async {
    _firebaseMessaging.getToken().then((value) {
      print('Push Token: $value');
      Provider.of<LoginBlock>(context).pushToken = value;
    });
    print('Changes By Ronak');
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        _showItemDialog(message, context);
      },
      onBackgroundMessage: myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        //_navigateToItemDetail(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        //_navigateToItemDetail(message);
      },
    );
  }

  Future<Null> _showItemDialog(
      Map<String, dynamic> message, BuildContext context) async {
    //final Item item = _itemForMessage(message);

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Item has been updated"),
            content: Text('strMessage'),
            actions: <Widget>[
              new FlatButton(
                child: Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });

    
  }

  Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }
  }
}

class MainTemplate extends StatelessWidget {
  final String title;
  final Widget child;

  MainTemplate({Key key, this.child, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(title, style: TextStyle(color: Preferences.textColor),),
        backgroundColor: Preferences.appColor,
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          Opacity(
            opacity: 0.6,
            child: Image.asset(
              'assets/images/bg.png',
              fit: BoxFit.fill,
            ),
          ),
          child
        ],
      ),
    );
  }
}
